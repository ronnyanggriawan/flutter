import 'package:flutter/material.dart';

import 'Config/ApiService.dart';
import 'model/Profile.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ApiService().getProfiles().then((value) => print("value: $value"));
    return MaterialApp(
      title: 'Welcome to Flutter',
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Welcome to Flutter'),
        ),
        body: const Center(
          child: const Text('Hello Word'),
        ),
      ),
    );
  }
}

