import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    TabBar myTabbar = TabBar(
      indicator: BoxDecoration(
          color: Colors.green,
          border: Border(top: BorderSide(color: Colors.amber, width: 6))),
      tabs: <Widget>[
        Tab(
          icon: Icon(Icons.comment),
          text: "Comments",
        ),
        Tab(
          icon: Icon(Icons.computer),
          text: "Computer",
        ),
      ],
    );

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: DefaultTabController(
          length: 2,
          child: Scaffold(
            appBar: AppBar(
                title: Text('Tab Bar'),
                bottom: PreferredSize(
                    preferredSize:
                        Size.fromHeight(myTabbar.preferredSize.height),
                    child: Container(color: Colors.red, child: myTabbar))),
            body: TabBarView(
              children: <Widget>[
                Center(
                  child: Text('Tab 1'),
                ),
                Center(
                  child: Text('Tab 2'),
                ),
              ],
            ),
          )),
    );
  }
}
