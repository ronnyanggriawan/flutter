import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  TextEditingController coba = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Latihan TextField atau edittext decoration'),
        ),
        body: Container(
          margin: EdgeInsets.all(20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              TextField(
                decoration: InputDecoration(
                    fillColor: Colors.blue[100],
                    filled: true,
                    icon: Icon(Icons.adb),
                    suffixIcon: Icon(Icons.person), //UNTUK DI BELAKANG
                    prefixIcon:
                        Icon(Icons.person), //UNTUK ICON DALAM KOTAK DEPAN
                    prefixText: 'Name :',
                    prefixStyle: TextStyle(
                        color: Colors.blue, fontWeight: FontWeight.w600),
                    labelText: 'Nama Lengkap',
                    hintText: 'Nama Lengkapnya lho...',
                    hintStyle: TextStyle(fontSize: 11, color: Colors.red),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    )),
                maxLength: 10,
                onChanged: (katro) {
                  setState(() {});
                },
                controller: coba,
              ),
              Text(coba.text)
            ],
          ),
        ),
      ),
    );
  }
}
