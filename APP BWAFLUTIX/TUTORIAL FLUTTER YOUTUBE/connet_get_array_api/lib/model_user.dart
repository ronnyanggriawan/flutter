import 'dart:convert';

import 'package:http/http.dart' as http;

class ResponseUser {
  String id, name;

  ResponseUser({this.id, this.name});

  factory ResponseUser.createResponUser(Map<String, dynamic> object) {
    return ResponseUser(
        id: object['id'].toString(),
        name: object['first_name'] + " " + object['last_name']);
  }

  static Future<List<ResponseUser>> getUser(String page) async {
    String apiURL = "https://reqres.in/api/users?page=" + page;
    var apiResult = await http.get(apiURL);
    var jsonObject = json.decode(apiResult.body);
    List<dynamic> listUser = (jsonObject as Map<String, dynamic>)['data'];

    List<ResponseUser> users = [];
    for (int i = 0; i < listUser.length; i++)
      users.add(ResponseUser.createResponUser(listUser[i]));

    return users;
  }
}
