import 'package:connet_get_array_api/model_user.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String datanya = 'Data Tidak Ada';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('API GET DEMO'),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Text(
                datanya,
                textAlign: TextAlign.center,
              ),
              RaisedButton(
                onPressed: () {
                  ResponseUser.getUser('2').then((value) {
                    datanya = '';
                    for (int i = 0; i < value.length; i++)
                      datanya = datanya + '[' + value[i].name + ']';
                    setState(() {});
                  });
                },
                child: Text('GET'),
              )
            ],
          ),
        ),
      ),
    );
  }
}
