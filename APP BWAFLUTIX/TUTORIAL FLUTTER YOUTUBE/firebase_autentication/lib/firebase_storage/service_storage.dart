import 'dart:io';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:path/path.dart';

class DatabaseFireStorage {
  static Future<String> uploadImage(File imageFile) async {
    String filename = basename(imageFile.path);

    StorageReference ref = FirebaseStorage.instance.ref().child("gambar").child(filename);
    StorageUploadTask task = ref.putFile(imageFile);
    StorageTaskSnapshot snapshot = await task.onComplete;

    return await snapshot.ref.getDownloadURL();
  }
}
