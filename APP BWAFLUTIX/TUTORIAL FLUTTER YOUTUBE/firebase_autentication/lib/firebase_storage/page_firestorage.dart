import 'dart:io';

import 'package:firebase_autentication/firebase_aut/service_aut/auth_services.dart';
import 'package:firebase_autentication/firebase_storage/service_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class FireStorageFirebase extends StatefulWidget {
  @override
  _FireStorageFirebaseState createState() => _FireStorageFirebaseState();
}

class _FireStorageFirebaseState extends State<FireStorageFirebase> {
  String imagePath;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Firebase Storage Demo"),
      ),
      body: Container(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              (imagePath != null)
                  ? Container(
                      width: 200,
                      height: 200,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(color: Colors.black),
                          image: DecorationImage(
                              image: NetworkImage(imagePath),
                              fit: BoxFit.cover)),
                    )
                  : Container(
                      width: 200,
                      height: 200,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(color: Colors.black)),
                    ),
              SizedBox(
                height: 10,
              ),
              RaisedButton(
                  child: Text("Sign In"),
                  onPressed: () async {
                    AuthServices.signInAnonymus();
                  }),
              RaisedButton(
                  child: Text("Upload Image"),
                  onPressed: () async {
                    File file = await getImage();
                    print(file);
                    imagePath = await DatabaseFireStorage.uploadImage(file);
                    setState(() {});
                  })
            ],
          ),
        ),
      ),
    );
  }
}

Future<File> getImage() async {
  return await ImagePicker.pickImage(source: ImageSource.gallery);
}
