import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_autentication/firebase_firestore/service_store/database_firestore.dart';
import 'package:flutter/material.dart';

class FireStoreFirebase extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
     return Scaffold(
       appBar: AppBar(
         title: Text("Firestore Demo"),
       ),
       body: Container(
         child: Center(
           child: Column(
             mainAxisAlignment: MainAxisAlignment.center,
             children: <Widget>[
               RaisedButton(
                   child: Text("Add Data"),
                   onPressed: () {
                     DatabaseServices.createOrIpdateProduct('1',
                         name: "Masker", price: 1000000);
                   }),
               RaisedButton(
                   child: Text("Edit Data"),
                   onPressed: () {
                     DatabaseServices.createOrIpdateProduct('1',
                         name: "Masker", price: 2000000);
                   }),
               RaisedButton(
                   child: Text("Get Data"),
                   onPressed: () async {
                     DocumentSnapshot snapShot =
                     await DatabaseServices.getProduct('1');
                     print(snapShot.data['nama']);
                     print(snapShot.data['harga']);
                   }),
               RaisedButton(
                   child: Text("Delete Data"),
                   onPressed: () async {
                     await DatabaseServices.deleteProduct("1");
                   }),
             ],
           ),
         ),
       ),
    );
  }
}
