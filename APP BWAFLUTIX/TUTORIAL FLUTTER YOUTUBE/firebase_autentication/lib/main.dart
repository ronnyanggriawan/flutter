import 'package:firebase_autentication/firebase_storage/page_firestorage.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'firebase_aut/service_aut/auth_services.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamProvider.value(
      value: AuthServices.firebaseUserStream,
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
// LOGIN
//        home: Wrapper(),
// FIRESTORE
//      home: FireStoreFirebase(),
//FIRESTORAGE
        home: FireStorageFirebase(),
      ),
    );
  }
}
