import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'file:///E:/ALL%20PROJECT/FLUTTER/flutter/APP%20BWAFLUTIX/TUTORIAL%20FLUTTER/firebase_autentication/lib/firebase_aut/main_page.dart';

import 'login_page.dart';

class Wrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    FirebaseUser firebaseUser = Provider.of<FirebaseUser>(context);
    return (firebaseUser == null) ? LoginPage() : MainPage(firebaseUser);
  }
}
