import 'file:///E:/ALL%20PROJECT/FLUTTER/flutter/APP%20BWAFLUTIX/TUTORIAL%20FLUTTER/firebase_autentication/lib/firebase_aut/service_aut/auth_services.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatelessWidget {
  final TextEditingController emailController = TextEditingController(text: "");
  final TextEditingController passwordController = TextEditingController(text: "");

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Login Page"),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Container(
              width: 300,
              height: 50,
              child: TextField(
                controller: emailController,
              ),
            ),
            Container(
              width: 300,
              height: 50,
              child: TextField(
                controller: passwordController,
              ),
            ),
            RaisedButton(
                child: Text("Login with Autentication"),
                onPressed: () async {
                  AuthServices.signInAnonymus();
                }),
            RaisedButton(
                child: Text("Sign Up"),
                onPressed: () async {
                  AuthServices.signUp(emailController.text, passwordController.text);
                }),
            RaisedButton(
                child: Text("Sign In"),
                onPressed: () async {
                  AuthServices.signIn(emailController.text, passwordController.text);
                }),
          ],
        ),
      ),
    );
  }
}
