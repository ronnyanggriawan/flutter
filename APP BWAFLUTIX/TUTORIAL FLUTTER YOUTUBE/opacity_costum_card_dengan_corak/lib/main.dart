import 'package:flutter/material.dart';
import 'package:opacity_costum_card_dengan_corak/dashboard.dart';

void main() {
  runApp(MyApp());
}
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Dashboard(),
    );
  }
}