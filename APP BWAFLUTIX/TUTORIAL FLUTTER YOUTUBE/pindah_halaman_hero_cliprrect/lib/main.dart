import 'package:flutter/material.dart';
import 'package:pindahhalamanherocliprrect/Dashboard.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(home: DashboardPage());
  }
}
