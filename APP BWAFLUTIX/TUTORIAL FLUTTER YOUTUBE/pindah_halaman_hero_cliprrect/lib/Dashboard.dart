import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pindahhalamanherocliprrect/Profile.dart';

class DashboardPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.lightBlue[50],
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text(
          'Animasi Hero',
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: Hero(
        tag: 'pp',
        child: GestureDetector(
          onTap: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) {
              return ProfilePage();
            }));
          },
          child: ClipRRect(
            borderRadius: BorderRadius.circular(50),
            child: Container(
              width: 100,
              height: 100,
              child: Image(
                  fit: BoxFit.cover,
                  image: NetworkImage(
                      'https://media.suara.com/pictures/970x544/2019/02/18/93949-enzy-storia-instagramatenzystoria.jpg')),
            ),
          ),
        ),
      ),
    );
  }
}
