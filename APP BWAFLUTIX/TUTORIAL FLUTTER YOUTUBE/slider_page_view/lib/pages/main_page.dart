import 'package:flutter/material.dart';
import 'package:slider_page_view/widgets/movie_box.dart';
/*
class MainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
     return Scaffold(
      appBar: AppBar(
        title: Text("title"),
      ),
      body: PageView(
        children: <Widget>[
          Container(color: Colors.green,),
          Container(color: Colors.blue,),
          Container(color: Colors.yellow,),
          Container(color: Colors.amber,),
        ],
      ),
    );
  }
}*/

class MainPage extends StatelessWidget {
  final List<String> url = [
    "https://i.pinimg.com/originals/92/c8/e0/92c8e00b34fcfdeaf605a0647c21adb3.jpg",
    "https://www.koalahero.com/wp-content/uploads/2019/04/2._Captain_America.jpg",
    "https://i.pinimg.com/originals/78/ac/aa/78acaad3c2890c0d47f94ec7b3cce9fb.jpg",
    "https://upload.wikimedia.org/wikipedia/id/f/fc/Thor_poster.jpg"
  ];
  final PageController controller =
      PageController(initialPage: 0, viewportFraction: 0.6);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text("Widget Slider"),
      ),
      body: PageView.builder(
        itemCount: url.length,
        controller: controller,
        itemBuilder: (context, index) => Center(
          child: MovieBox(url[index]),
        ),
      ),
    );
  }
}
