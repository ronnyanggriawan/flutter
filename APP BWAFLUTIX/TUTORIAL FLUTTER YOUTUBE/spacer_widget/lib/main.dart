import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Spacer Widget'),
        ),
        body: Center(
          child: Row(
            // mainAxisAlignment: MainAxisAlignment.spaceEvenly, //INI CARA MUDAH UNTUK MEMBAGI CONTAINER
            children: <Widget>[
              Container(color: Colors.green, width: 80, height: 80,),
              Spacer(flex: 1,),
              Container(color: Colors.red, width: 80, height: 80,),
              Spacer(flex: 2,),
              Container(color: Colors.black, width: 80, height: 80,),
              Spacer(flex: 3,),
            ],
          ),
        ),
      ),
    );
  }
}
