import 'package:mobx/mobx.dart';
part 'counter.g.dart';

class CounterMobX = _CounterMobX with _$CounterMobX;

abstract class _CounterMobX with Store {
  @observable
  int value = 0;

  @action
  void increment(){
    value++;
  }

  @action
  void decrement(){
    value--;
  }
}
