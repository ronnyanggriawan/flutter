import 'package:flutter/widgets.dart';

class TimeState with ChangeNotifier {
  int _time = 15;

  int get time => _time;

  set time(int value) {
    _time = value;
    notifyListeners();
  }
}
