import 'dart:convert';

import 'package:http/http.dart' as http;

class ResponseUser {
  String id, name;

  ResponseUser({this.id, this.name});

  factory ResponseUser.createResponUser(Map<String, dynamic> object) {
    return ResponseUser(
        id: object['id'].toString(),
        name: object['first_name'] + " " + object['last_name']);
  }

  static Future<ResponseUser> connectToAPI(String id) async {
    String apiURL = "https://reqres.in/api/users/" + id;

    var apiResponse = await http.get(apiURL);
    var jsonObject = json.decode(apiResponse.body);
    var userData = (jsonObject as Map<String, dynamic>)['data'];
    return ResponseUser.createResponUser(userData);
  }
}
