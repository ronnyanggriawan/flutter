import 'package:connet_get_api/Model_user.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  ResponseUser responseUser;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('API GET DEMO'),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Text(
                (responseUser != null)
                    ? responseUser.id + " | " + responseUser.name + " | "
                    : "Respon kosong.",
                textAlign: TextAlign.center,
              ),
              RaisedButton(
                onPressed: () {
                  ResponseUser.connectToAPI('5').then((value) {
                    responseUser = value;
                    setState(() {});
                  });
                },
                child: Text('GET'),
              )
            ],
          ),
        ),
      ),
    );
  }
}
