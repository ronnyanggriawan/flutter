import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
String message = 'ini adalah text';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Anonymous Metod'),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(message),
              RaisedButton(child: Text('tekan saya'), 
              onPressed: (){
                setState(() {
                  message = 'Tombol sudah Ditekan'; //METODENYA LANGSUNG DI ONPRESSNYA ATAU BUTTONNYA
                });
              })
            ],
          ),
        ),
      ),
    );
  }
}
