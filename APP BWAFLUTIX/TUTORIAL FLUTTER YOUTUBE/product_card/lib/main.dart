import 'package:flutter/material.dart';
import 'package:product_card/product_card.dart';
import 'package:product_card/product_state.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text('Product Card'),
          backgroundColor: firstColor,
        ),
        body: ChangeNotifierProvider<ProductState>(
          create: (context) => ProductState(),
          child: Container(
            margin: EdgeInsets.all(20),
            child: Align(
              alignment: Alignment.topCenter,
              child: Consumer<ProductState>(
                builder: (context, productState, _) => ProductCard(
                  imageURL:
                      'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/assortment-of-colorful-ripe-tropical-fruits-top-royalty-free-image-995518546-1564092355.jpg',
                  name: 'Buah-buahan Mix 1 Kg',
                  price: 'Rp25.000',
                  onAddCartTap: () {},
                  quantity: productState.quantity,
                  onIncTap: () {
                    productState.quantity++;
                  },
                  onDecTap: () {
                    if (productState.quantity > 0) productState.quantity--;
                  },
                  notification:
                      (productState.quantity > 5) ? 'Diskon 10%' : null,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
