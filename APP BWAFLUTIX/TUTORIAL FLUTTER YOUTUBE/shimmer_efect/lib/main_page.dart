import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class MainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Shimmer demo"),
      ),
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Stack(
            children: <Widget>[
              Container(
                width: 200,
                height: 300,
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.black),
                    image: DecorationImage(
                        image: NetworkImage(
                            "https://1.bp.blogspot.com/-nseZiw2QQz0/XM6qNGsUaYI/AAAAAAAAF0o/wuLoywejA0E3rmU4q-jV_grbIf_OrweWwCLcBGAs/s1600/Sifat%2BBuruk%2BSon%2BGoku%2Bdalam%2BAnime%2BDragon%2BBall%2Byang%2BTak%2BPatut%2BDitiru.jpg"),
                        fit: BoxFit.cover)),
              ),
              Shimmer(
                gradient: LinearGradient(
                    begin: Alignment(1, 1),
                    end: Alignment(-1, -1),
                    stops: [
                      0.45,
                      0.5,
                      0.55
                    ],
                    colors: [
                      Colors.blue.withOpacity(0),
                      Colors.blue.withOpacity(0.5),
                      Colors.blue.withOpacity(0)
                    ]),
                child: Container(
                  width: 200,
                  height: 300,
                  color: Colors.red,
                ),
              )
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Shimmer(
            period: Duration(seconds: 3),
            direction: ShimmerDirection.btt,
            gradient: LinearGradient(
                begin: Alignment.bottomRight,
                end: Alignment.topRight,
                stops: [
                  0.45,
                  0.5,
                  0.55
                ],
                colors: [
                  Colors.blue,
                  Colors.white,
                  Colors.blue
                ]),
            child: Text(
              "Dragon Ball",
              style: TextStyle(
                  color: Colors.blue,
                  fontSize: 30,
                  fontWeight: FontWeight.bold),
            ),
          )
        ],
      )),
    );
  }
}
