import 'package:flutter/material.dart';
import 'package:qrscan/qrscan.dart';
import 'package:simple_permissions/simple_permissions.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String munyuk = 'QR Code Data';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('QR Scan'),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Text(
                munyuk,
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600),
              ),
              RaisedButton(
                onPressed: () {scanQR();},
                child: Text('Scan QR'),
              )
            ],
          ),
        ),
      ),
    );
  }

  void scanQR() async {
    bool hasil = await SimplePermissions.checkPermission(Permission.Camera);
    PermissionStatus status = PermissionStatus.notDetermined;

    if (!hasil)
      status = await SimplePermissions.requestPermission(Permission.Camera);

    if (hasil || status == PermissionStatus.authorized) {
      String hasilScan = await scan();
      setState(() {
        munyuk = hasilScan;
      });
    }
  }
}
