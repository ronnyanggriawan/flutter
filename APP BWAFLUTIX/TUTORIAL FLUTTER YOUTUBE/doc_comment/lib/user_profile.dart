import 'package:flutter/material.dart';

/// Class UserPRofile di gunakan untuk menampilkan profile dengan layar indah
class UserProfile extends StatelessWidget {
  ///field ini digunakan untuk menyimpan nama user
  final String name, role, photoURL;

  /// * [nama] berisi *nama user*. Nilai defaultnya adalah `No Name`
  ///
  /// * [role] berisi peran/jabatan dari user. Nilai defaultnya adalah `No Role`
  ///
  /// * [photoURL] berisi **foto user**. Nilai defaultnya adalah `Null`
  /// 
  /// Contoh :
  /// 
  /// ```
  /// final UserProfile profile = UserProfile(
    ///name: 'nama user',
    ///role: 'peran user',
    ///photoURL: 'http://lalal/com',
  ///);
  ///```
  UserProfile({this.name = 'No Name', this.role = 'No Role', this.photoURL});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Image(
          image: NetworkImage((photoURL != null)
              ? photoURL
              : 'https://www.pngfind.com/pngs/m/470-4703547_icon-user-icon-hd-png-download.png'),
          fit: BoxFit.contain,
          width: 200,
          height: 200,
        ),
        Text(
          name,
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
        ),
        Text(
          '[' + role + ']',
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
        )
      ],
    );
  }
}
