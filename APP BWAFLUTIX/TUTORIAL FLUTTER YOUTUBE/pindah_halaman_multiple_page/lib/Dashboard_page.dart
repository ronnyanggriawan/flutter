import 'package:flutter/material.dart';
import 'package:pindah_halaman_multiple_page/Profile_Page.dart';

class DashboardPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Dashboard'),
        ),
        body: Center(
          child: RaisedButton(child: Text('Profile'), onPressed: (){
            Navigator.push(context, MaterialPageRoute(builder: (context){
              return ProfilePage();
            }));
          }),
        ),
      ),
    );
  }
}
