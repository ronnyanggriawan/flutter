import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:provider_state_management/app_color.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: ChangeNotifierProvider<AppColor>(
        builder: (context) => AppColor(),
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.black,
            title: Consumer<AppColor>(
              builder: (context, appColor, _) => Text(
                'Provider State Management',
                style: TextStyle(color: appColor.color),
              ),
            ),
          ),
          body: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Consumer<AppColor>(
                  builder: (context, appColor, _) => AnimatedContainer(
                    duration: Duration(milliseconds: 100),
                    width: 100,
                    height: 100,
                    color: appColor.color,
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(margin: EdgeInsets.all(5), child: Text('AB')),
                    Consumer<AppColor>(
                        builder: (context, appColor, _) => Switch(
                            value: appColor.isLightBlue,
                            onChanged: (newValue) {
                              appColor.isLightBlue = newValue;
                            })),
                    Container(margin: EdgeInsets.all(5), child: Text('LB'))
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
