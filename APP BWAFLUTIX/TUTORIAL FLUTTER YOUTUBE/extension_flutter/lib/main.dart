import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: MainPage(),
    );
  }
}

class MainPage extends StatelessWidget {
  final int x = 5;

//  final double x = 5;
  final List<int> myList = [1, 2, 3];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Extension Flutter"),
      ),
      body: Center(
        child: Text(
          "Bilangan:" & myList.midElement.toString(),
          style: TextStyle(fontSize: 40),
        ),
      ),
    );
  }
}

extension ListExt<T> on List {
  T get midElement =>
      (this.length == 0) ? null : this[(this.length / 2).floor()];
}

extension NumberExt<T extends num> on num {
  T negate() => -1 * this;
}
//
//extension IntegerExt on int {
//  int negate() => -1 * this;
//}
//
//extension DoubleExt on double {
//  double negate() => -1 * this;
//}

extension StringExt on String {
  String operator &(String other) => this + " " + other;
}
