import 'dart:math';

import 'package:flutter/material.dart';
import 'package:slider_page_view_transition/widgets/movie_box.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  final List<String> url = [
    "https://i.pinimg.com/originals/92/c8/e0/92c8e00b34fcfdeaf605a0647c21adb3.jpg",
    "https://www.koalahero.com/wp-content/uploads/2019/04/2._Captain_America.jpg",
    "https://i.pinimg.com/originals/78/ac/aa/78acaad3c2890c0d47f94ec7b3cce9fb.jpg",
    "https://upload.wikimedia.org/wikipedia/id/f/fc/Thor_poster.jpg"
  ];

  final PageController controller =
      PageController(initialPage: 0, viewportFraction: 0.6);

  double currentPageValue = 0;

  @override
  void initState() {
    super.initState();
    controller.addListener(() {
      setState(() {
        currentPageValue = controller.page;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text("Widget Slider"),
      ),
      body: PageView.builder(
        itemCount: url.length,
        controller: controller,
        itemBuilder: (context, index) {
          double diference = index - currentPageValue;
          if (diference < 0) {
            diference *= -1;
          }
          diference = min(1, diference);
          return Center(
            child: MovieBox(
              url[index],
              scale: 1 - (diference * 0.3),
            ),
          );
        },
      ),
    );
  }
}
