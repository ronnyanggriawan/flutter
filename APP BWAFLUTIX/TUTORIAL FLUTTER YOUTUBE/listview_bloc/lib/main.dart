import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:listview_bloc/product_bloc.dart';
import 'product_card.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home:
          BlocProvider(builder: (context) => ProductBloc(), child: MainPage()),
    );
  }
}

class MainPage extends StatelessWidget {
  final Random r = Random();

  @override
  Widget build(BuildContext context) {
    ProductBloc bloc = BlocProvider.of<ProductBloc>(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: firstColor,
        title: Text('Demo ListView Builder'),
      ),
      body: Column(
        children: <Widget>[
          RaisedButton(
            color: firstColor,
              child: Text('Create Product',style: TextStyle(color: Colors.white),),onPressed: () {
            bloc.dispatch(r.nextInt(4) + 2);
          }),
          SizedBox(height: 20,),
          BlocBuilder<ProductBloc, List<Product>>(
            builder: (context, product) => Expanded(
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) {
                  return Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      (index == 0)? SizedBox(width: 20,): Container(),
                      ProductCard(
                        imageURL: product[index].imageURL,
                        name: product[index].name,
                        price: product[index].price.toString(),
                        onAddCartTap: () {},
                        onIncTap: () {},
                        onDecTap: () {},
                      ),
                      SizedBox(width: 20,)
                    ],
                  );
                },
                itemCount: product.length,
              ),
            ),
          )
        ],
      ),
    );
  }
}
