import 'package:bloc/bloc.dart';

class Product {
  String imageURL, name;
  int price;

  Product({this.imageURL = '', this.name = '', this.price = 0});
}

class ProductBloc extends Bloc<int, List<Product>> {
  @override
  List<Product> get initialState => [];

  @override
  Stream<List<Product>> mapEventToState(int event) async* {
    List<Product> product = [];
    for (int i = 0; i < event; i++)
      product.add(Product(
          imageURL:
              'https://asset.kompas.com/crops/DAT0bF9Dp21QqlgqXD-UcsBpxY4=/20x0:992x648/750x500/data/photo/2018/01/14/2342554562.jpg',
          name: 'Product' + i.toString(),
          price: (i + 1) * 5000));
    yield product;
  }
}
