import 'package:division/division.dart';
import 'package:division_css/style/custom_style.dart';
import 'package:flutter/material.dart';

class CustomButton extends StatefulWidget {
  final ParentStyle buttonStyle;

  const CustomButton(this.buttonStyle);

  @override
  _CustomButtonState createState() => _CustomButtonState();
}

class _CustomButtonState extends State<CustomButton> {
  bool isTapDown = false;

  @override
  Widget build(BuildContext context) {
    return Parent(
      child: Container(
        child: Txt(
          "division",
          style: CustomStyle.txtStyle,
        ),
      ),
      style: widget.buttonStyle
        ..clone()
        ..scale((isTapDown) ? 1.1 : 1)
        ..elevation((isTapDown) ? 0 : 5),
      gesture: Gestures()
        ..onTapDown((details) {
          setState(() {
            isTapDown = true;
          });
        })
        ..onTapUp((detail) {
          setState(() {
            isTapDown = false;
          });
        })
        ..onTapCancel(() {
          setState(() {
            isTapDown = false;
          });
        }),
    );
  }
}
