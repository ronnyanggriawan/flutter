import 'package:division/division.dart';
import 'package:division_css/custom_button.dart';
import 'package:division_css/style/custom_style.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: MainPage(),
    );
  }
}

class MainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Txt(
          "Divison CSS",
          style: CustomStyle.txtStyle.clone()..fontSize(18),
        ),
        backgroundColor: Colors.red,
      ),
      backgroundColor: Colors.grey,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            CustomButton(CustomStyle.buttonStyle),
            SizedBox(
              height: 20,
            ),
            CustomButton(CustomStyle.buttonStyle.clone()
              ..background.color(Colors.green)
              ..border(all: 3, color: Colors.green[900]))
          ],
        ),
      ),
    );
  }
}
