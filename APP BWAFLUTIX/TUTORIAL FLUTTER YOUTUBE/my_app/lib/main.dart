import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var scaffold = Scaffold(
      appBar: AppBar(
        title: Center(child: Text('Aplikasi Hello Word')),
      ),
      body: Center(
          child: Container(
              color: Colors.lightBlue, // warna kotak
              width: 150,
              height: 100,
              child: Text(
                'Saya sendang melatih kemampuan flutter saya.',
                style: TextStyle(
                    color: Colors.white, //warna text
                    fontStyle: FontStyle.italic,
                    fontWeight: FontWeight.w700,
                    fontSize: 20),
              ))),
    );
    return MaterialApp(
      home: scaffold,
    );
  }
}
