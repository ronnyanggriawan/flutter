import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: MainPage(),
    );
  }
}

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  Person selectPerson;

  List<Person> person = [Person("Joni"), Person("Joko")];

  List<DropdownMenuItem> generateItem(List<Person> person) {
    List<DropdownMenuItem> items = [];
    for (var itemnya in person) {
      items.add(DropdownMenuItem(
        child: Text(itemnya.name),
        value: itemnya,
      ));
    }
    return items;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Spinner Atau DropDown"),
      ),
      body: Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.all(20),
            child: DropdownButton(
              isExpanded: true,
              style: TextStyle(fontSize: 20,color: Colors.purple),
              items: generateItem(person),
              onChanged: (item) {
                setState(() {
                  selectPerson = item;
                });
              },
              value: selectPerson,
            ),
          ),
          Text((selectPerson != null)
              ? selectPerson.name
              : "Belum Ada yang terpilih",style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold ),)
        ],
      ),
    );
  }
}

class Person {
  String name;

  Person(this.name);
}
