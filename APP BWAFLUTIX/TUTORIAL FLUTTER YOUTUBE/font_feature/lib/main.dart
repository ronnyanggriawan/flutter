import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Flutter Typography'),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Text(
                'Contoh 01 (Tanpa Apapun)',
                style: TextStyle(fontSize: 20),
              ),
              Text(
                'Contoh 02 (Small Caps)',
                style: TextStyle(
                    fontSize: 20, fontFeatures: [FontFeature.enable('smcp')]),
              ),
              Text(
                'Contoh 03 1/2 (Frac & Small Caps)',
                style: TextStyle(fontSize: 20, fontFeatures: [
                  FontFeature.enable('frac'),
                  FontFeature.enable('smcp')
                ]),
              ),
              Text(
                'Milonga 3 1/2 (Milonga)',
                style: TextStyle(fontSize: 20,fontFamily: 'Milonga-Reguler', fontFeatures: [
                  FontFeature.enable('smcp'),
                  FontFeature.enable('frac')
                ]),
              ),
              Text(
                'Contoh Cardo 19 (Default Cardo)',
                style: TextStyle(fontSize: 20, fontFamily: 'Cardo-Regular'),
              ),
              Text(
                'Contoh Cardo 19 (Frac & Small Caps)',
                style: TextStyle(
                    fontSize: 20,
                    fontFamily: 'Cardo-Regular',
                    fontFeatures: [
                      FontFeature.oldstyleFigures(),
                    ]),
              ),
              Text(
                'Gabriola (Default)',
                style: TextStyle(
                    fontSize: 20,
                    fontFamily: 'Gabriola.ttf',),
              ),
              Text(
                'Gabriola (Style set nomor 5)',
                style: TextStyle(
                    fontSize: 20,
                    fontFamily: 'Gabriola',
                    fontFeatures: [
                      FontFeature.stylisticSet(5),
                    ]),
              )
            ],
          ),
        ),
      ),
    );
  }
}
