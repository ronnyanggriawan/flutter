import 'package:button_costum_belah_ketupat/Colorfull_Button.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Colorful Button'),
        ),
        body: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              ColorfulButton(Colors.red,Colors.blue,Icons.adb),
              ColorfulButton(Colors.yellow,Colors.green,Icons.computer),
              ColorfulButton(Colors.purple,Colors.amber,Icons.comment),
              ColorfulButton(Colors.orange,Colors.green,Icons.person),
            ],
          ),
        ),
      ),
    );
  }
}

