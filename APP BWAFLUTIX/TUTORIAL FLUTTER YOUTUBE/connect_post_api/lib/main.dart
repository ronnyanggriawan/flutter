import 'package:connect_post_api/post_result_model.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  ResponsePost responsePost;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('API POST DEMO'),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Text((responsePost != null)
                  ? responsePost.id +
                      " | " +
                      responsePost.name +
                      " | " +
                      responsePost.job +
                      " | " +
                      responsePost.created
                  : "Respon kosong.",textAlign: TextAlign.center,),
              RaisedButton(
                onPressed: () {
                  setState(() {
                    ResponsePost.connectToAPI('ronny anggriawan', 'Programmer')
                        .then((value) => responsePost = value);
                  });
                },
                child: Text('POST'),
              )
            ],
          ),
        ),
      ),
    );
  }
}
