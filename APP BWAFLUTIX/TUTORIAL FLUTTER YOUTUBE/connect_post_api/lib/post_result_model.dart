import 'dart:convert';

import 'package:http/http.dart' as http;

class ResponsePost {
  String id, name, job, created;

  ResponsePost({this.id, this.name, this.job, this.created});

  factory ResponsePost.createResponsePost(Map<String, dynamic> object) {
    return ResponsePost(
        id: object['id'],
        name: object['name'],
        job: object['job'],
        created: object['createdAt']);
  }

  static Future<ResponsePost> connectToAPI(String name,String job) async{
    String apiURL = "https://reqres.in/api/users";

    var apiResponse = await http.post(apiURL, body: {
      "name": name , "job" : job
    });

    var jsonObject = json.decode(apiResponse.body);

    return ResponsePost.createResponsePost(jsonObject);
  }
}
