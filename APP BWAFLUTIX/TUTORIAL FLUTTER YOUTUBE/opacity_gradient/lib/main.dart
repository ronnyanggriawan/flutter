import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Gradient Opacity'),
        ),
        body: Center(
          child: ShaderMask(
            shaderCallback: (rectangle) {
              return LinearGradient(
                      colors: [Colors.black, Colors.transparent],
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter)
                  .createShader(
                      Rect.fromLTRB(0, 0, rectangle.width, rectangle.bottom));
            },
            blendMode: BlendMode.dstIn,
            child: Image(
                width: 400,
                image: NetworkImage(
                    'https://eksotisjogja.com/wp-content/uploads/2016/08/pantai-siung.jpg')),
          ),
        ),
      ),
    );
  }
}
