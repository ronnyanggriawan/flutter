import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  List<bool> pencet = [true, false, false];
  ColorFilter colorFilter = ColorFilter.mode(Colors.blue, BlendMode.screen);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: ColorFiltered(
        colorFilter: colorFilter,
        child: Scaffold(
          appBar: AppBar(
            title: Text('SelectTable, ToggleButton dan ColorFilter'),
          ),
          body: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SelectableText(
                  'Ini adalah selectable text. Silahkan pilih saya.',
                  style: TextStyle(fontSize: 20),
                  showCursor: true,
                  cursorColor: Colors.red,
                  cursorWidth: 4,
                ),
                SizedBox(
                  height: 5,
                ),
                ToggleButtons(
                  children: <Widget>[
                    Icon(Icons.access_alarm),
                    Icon(Icons.adb),
                    Icon(Icons.add_comment),
                  ],
                  isSelected: pencet,
                  onPressed: (index) {
                    setState(() {
                      if (index == 0)
                        colorFilter =
                            ColorFilter.mode(Colors.blue, BlendMode.screen);
                      else if (index == 1)
                        colorFilter =
                            ColorFilter.mode(Colors.green, BlendMode.softLight);
                      else
                        colorFilter =
                            ColorFilter.mode(Colors.purple, BlendMode.multiply);
                      for (int i = 0; i < pencet.length; i++)
                        pencet[i] = (i == index) ? true : false;
//                    pencet[index] = !pencet[index];
                    });
                  },
                  fillColor: Colors.red[50],
                  selectedColor: Colors.red,
                  selectedBorderColor: Colors.red,
                  splashColor: Colors.blue,
                  highlightColor: Colors.yellow,
                  borderRadius: BorderRadius.circular(15),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
