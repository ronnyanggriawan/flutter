import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Color color1 = Colors.red;
  Color color2 = Colors.amber;
  Color targetColor;
  bool keterima = false;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Dragable'),
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                //LINGKARANG KESATU
                Draggable<Color>(
                  data: color1,
                  child: SizedBox(
                    width:
                        50, //KALAU DI GANTI JADI ANGKA LEBIH BESAR AKAN JADI LONJONG
                    height: 50,
                    child: Material(
                      color: color1,
                      shape: StadiumBorder(),
                      elevation: 3,
                    ),
                  ),
                  childWhenDragging: SizedBox(
                    width:
                        50, //KALAU DI GANTI JADI ANGKA LEBIH BESAR AKAN JADI LONJONG
                    height: 50,
                    child: Material(
                      color: Colors.black26,
                      shape: StadiumBorder(),
                    ),
                  ),
                  feedback: SizedBox(
                    width:
                        50, //KALAU DI GANTI JADI ANGKA LEBIH BESAR AKAN JADI LONJONG
                    height: 50,
                    child: Material(
                      color: color1.withOpacity(0.7),
                      shape: StadiumBorder(),
                      elevation: 3,
                    ),
                  ),
                ),
                //LINGKARAN KE 2
                Draggable<Color>(
                  data: color2,
                  child: SizedBox(
                    width:
                        50, //KALAU DI GANTI JADI ANGKA LEBIH BESAR AKAN JADI LONJONG
                    height: 50,
                    child: Material(
                      color: color2,
                      shape: StadiumBorder(),
                      elevation: 3,
                    ),
                  ),
                  childWhenDragging: SizedBox(
                    width:
                        50, //KALAU DI GANTI JADI ANGKA LEBIH BESAR AKAN JADI LONJONG
                    height: 50,
                    child: Material(
                      color: Colors.black26,
                      shape: StadiumBorder(),
                    ),
                  ),
                  feedback: SizedBox(
                    width:
                        50, //KALAU DI GANTI JADI ANGKA LEBIH BESAR AKAN JADI LONJONG
                    height: 50,
                    child: Material(
                      color: color2.withOpacity(0.7),
                      shape: StadiumBorder(),
                      elevation: 3,
                    ),
                  ),
                ),
              ],
            ),

            // KERANJANG ATAU TAGER DRAGNYA
            DragTarget<Color>(
              onWillAccept: (value) => true,
              onAccept: (value) {keterima = true;targetColor = value;},
              builder: (context, candidates, rejected) {
               return (keterima) ? SizedBox(
                        width: 100,
                        height: 100,
                        child: Material(
                          color: targetColor,
                          shape: StadiumBorder(),
                        ),
                      ):
                       SizedBox(
                        width: 100,
                        height: 100,
                        child: Material(
                          color: Colors.black26,
                          shape: StadiumBorder(),
                        ),
                      );
              },
            )
          ],
        ),
      ),
    );
  }
}
