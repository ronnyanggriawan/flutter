import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.green,
        body: Container(
          margin: EdgeInsets.all(5),
          child: ListView(
            children: <Widget>[
              // buildCard(Icons.account_box, 'Account Box'),
              // buildCard(Icons.adb, 'Serangga Android')
              buildCard(),
              buildCard()
            ],
          ),
        ),
      ),
    );
  }

  // Card buildCard(IconData iconData, String text) {
  //   return Card(
  //     elevation: 5,
  //     child: Row(
  //       children: <Widget>[
  //         Container(
  //             margin: EdgeInsets.all(5),
  //             child: Icon(
  //               iconData,
  //               color: Colors.blue,
  //             )),
  //         Text(text)
  //       ],
  //     ),
  //   );
  // }

  Card buildCard() {
    return Card(
      elevation: 5,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Column(
            children: <Widget>[
              Container(
                  margin: EdgeInsets.all(5), child: Icon(Icons.account_box)),
              Text('Account Box')
            ],
          ),
          Row(
            children: <Widget>[
              Container(
                  margin: EdgeInsets.all(5), child: Icon(Icons.account_box)),
              Text('Account Box')
            ],
          )
        ],
      ),
    );
  }
}
