part of 'models.dart';

class User extends Equatable {
  final String id, email, name, profilePicture, selectedLanguage;
  final int balance;
  final List<String> selectedGenres;

  User(this.id, this.email,
      {this.name,
      this.profilePicture,
      this.selectedLanguage,
      this.balance,
      this.selectedGenres});

  @override
  String toString() {
    return "[$id] - $name,$email";
  }

  @override
  List<Object> get props => [
        id,
        email,
        name,
        profilePicture,
        selectedGenres,
        selectedLanguage,
        balance
      ];
}
