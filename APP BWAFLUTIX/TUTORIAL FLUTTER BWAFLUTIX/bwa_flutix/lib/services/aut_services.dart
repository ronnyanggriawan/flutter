part of 'services.dart';

class AutServices {
  static FirebaseAuth _auth = FirebaseAuth.instance;

  static Future<SignInSigUpResult> signUp(String email, String password,
      String name, List<String> selectedGenres, String selectedLanguage) async {
    try {
      AuthResult result = await _auth.createUserWithEmailAndPassword(
          email: email, password: password);

      User userModel = result.user.convertToUser(
          name: name,
          selectedGenres: selectedGenres,
          selectedLanguage: selectedLanguage);

      await UserServices.updateUser(userModel);
      return SignInSigUpResult(user: userModel);
    } catch (e) {
      return SignInSigUpResult(message: e.toString().split(',')[1]);
    }
  }

  static Future<SignInSigUpResult> signIn(String email, String password) async {
    try {
      AuthResult result = await _auth.signInWithEmailAndPassword(
          email: email, password: password);
      User user = await result.user.fromFireStore();
      return SignInSigUpResult(user: user);
    } catch (e) {
      return SignInSigUpResult(message: e.toString().split(',')[1]);
    }
  }

  static Future<void> signOut() async {
    await _auth.signOut();
  }

  static Stream<FirebaseUser> get userStream => _auth.onAuthStateChanged;
}

class SignInSigUpResult {
  final User user;
  final String message;

  SignInSigUpResult({this.user, this.message});
}
